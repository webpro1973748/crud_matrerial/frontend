import { Test, TestingModule } from '@nestjs/testing';
import { MatrerialsService } from './matrerials.service';

describe('MatrerialsService', () => {
  let service: MatrerialsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MatrerialsService],
    }).compile();

    service = module.get<MatrerialsService>(MatrerialsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
