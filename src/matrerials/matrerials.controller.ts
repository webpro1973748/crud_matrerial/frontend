import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MatrerialsService } from './matrerials.service';
import { CreateMatrerialDto } from './dto/create-matrerial.dto';
import { UpdateMatrerialDto } from './dto/update-matrerial.dto';

@Controller('matrerial')
export class MatrerialsController {
  constructor(private readonly matrerialsService: MatrerialsService) {}
  // Create
  @Post()
  create(@Body() createMatrerialDto: CreateMatrerialDto) {
    return this.matrerialsService.create(createMatrerialDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.matrerialsService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.matrerialsService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMatrerialDto: UpdateMatrerialDto,
  ) {
    return this.matrerialsService.update(+id, updateMatrerialDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.matrerialsService.remove(+id);
  }
}
