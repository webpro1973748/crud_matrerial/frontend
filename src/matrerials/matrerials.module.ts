import { Module } from '@nestjs/common';
import { MatrerialsService } from './matrerials.service';
import { MatrerialsController } from './matrerials.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Matrerial } from './entities/matrerial.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Matrerial])],
  controllers: [MatrerialsController],
  providers: [MatrerialsService],
})
export class MatrerialsModule {}
