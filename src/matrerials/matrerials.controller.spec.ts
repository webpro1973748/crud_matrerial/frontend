import { Test, TestingModule } from '@nestjs/testing';
import { MatrerialsController } from './matrerials.controller';
import { MatrerialsService } from './matrerials.service';

describe('MatrerialsController', () => {
  let controller: MatrerialsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MatrerialsController],
      providers: [MatrerialsService],
    }).compile();

    controller = module.get<MatrerialsController>(MatrerialsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
