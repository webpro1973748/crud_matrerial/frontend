import { Injectable } from '@nestjs/common';
import { CreateMatrerialDto } from './dto/create-matrerial.dto';
import { UpdateMatrerialDto } from './dto/update-matrerial.dto';
import { Matrerial } from './entities/matrerial.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MatrerialsService {
  constructor(
    @InjectRepository(Matrerial)
    private matrerialsRepository: Repository<Matrerial>,
  ) {}
  create(createMatrerialDto: CreateMatrerialDto): Promise<Matrerial> {
    return this.matrerialsRepository.save(createMatrerialDto);
  }

  findAll(): Promise<Matrerial[]> {
    return this.matrerialsRepository.find();
  }

  findOne(id: number) {
    return this.matrerialsRepository.findOneBy({ id: id });
  }

  async update(id: number, updateMatrerialDto: UpdateMatrerialDto) {
    this.matrerialsRepository.update(id, updateMatrerialDto);
    const matrerial = await this.matrerialsRepository.findOneBy({ id });
    return matrerial;
  }

  async remove(id: number) {
    const deleteMatrerial = await this.matrerialsRepository.findOneBy({ id });
    return this.matrerialsRepository.remove(deleteMatrerial);
  }
}
