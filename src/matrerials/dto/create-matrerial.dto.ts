import { IsNotEmpty, Length } from 'class-validator';
export class CreateMatrerialDto {
  @IsNotEmpty()
  @Length(4, 32)
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  quantity: number;

  @IsNotEmpty()
  min: number;

  @IsNotEmpty()
  status: 'Available' | 'Low';
}
