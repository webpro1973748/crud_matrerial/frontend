import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Matrerial {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  quantity: number;

  @Column()
  min: number;

  @Column()
  status: 'Available' | 'Low';
}
